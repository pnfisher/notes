curl
aspell
xinit
cygrunsrv
openssh

mkpasswd -l -c > /etc/passwd
mkgroup -l -d > /etc/group

Set /etc/nsswitch.conf to

  passwd: files db

mount -o noacl //brome/phil /home/phil/mnt


## setting up sshd

# clenaup old install
TP-20141493:~ % cygrunsrv --stop sshd
TP-20141493:~ % cygrunsrv --remove sshd
TP-20141493:~ % rm -rf /var/empty
TP-20141493:~ % net user sshd /delete
The user name could not be found.

More help is available by typing NET HELPMSG 2221.

TP-20141493:~ % net user cyg_server /delete
The user name could not be found.

More help is available by typing NET HELPMSG 2221.

# now reinstall
TP-20141493:~ % ssh-host-config

*** Info: Generating missing SSH host keys
*** Query: Overwrite existing /etc/ssh_config file? (yes/no) yes
*** Info: Creating default /etc/ssh_config file
*** Query: Overwrite existing /etc/sshd_config file? (yes/no) yes
*** Info: Creating default /etc/sshd_config file

*** Info: StrictModes is set to 'yes' by default.
*** Info: This is the recommended setting, but it requires that the POSIX
*** Info: permissions of the user's home directory, the user's .ssh
*** Info: directory, and the user's ssh key files are tight so that
*** Info: only the user has write permissions.
*** Info: On the other hand, StrictModes don't work well with default
*** Info: Windows permissions of a home directory mounted with the
*** Info: 'noacl' option, and they don't work at all if the home
*** Info: directory is on a FAT or FAT32 partition.
*** Query: Should StrictModes be used? (yes/no) no
*** Info: Note that creating a new user requires that the current account have
*** Info: Administrator privileges.  Should this script attempt to create a
*** Query: new local account 'sshd'? (yes/no) yes
*** Info: Updating /etc/sshd_config file
*** Query: Overwrite existing /etc/inetd.d/sshd-inetd file? (yes/no) yes
*** Info: Creating default /etc/inetd.d/sshd-inetd file
*** Info: Updated /etc/inetd.d/sshd-inetd

*** Query: Do you want to install sshd as a service?
*** Query: (Say "no" if it is already installed as a service) (yes/no) yes
*** Query: Enter the value of CYGWIN for the daemon: []
*** Info: On Windows Server 2003, Windows Vista, and above, the
*** Info: SYSTEM account cannot setuid to other users -- a capability
*** Info: sshd requires.  You need to have or to create a privileged
*** Info: account.  This script will help you do so.

*** Info: It's not possible to use the LocalSystem account for services
*** Info: that can change the user id without an explicit password
*** Info: (such as passwordless logins [e.g. public key authentication]
*** Info: via sshd) when having to create the user token from scratch.
*** Info: For more information on this requirement, see
*** Info: https://cygwin.com/cygwin-ug-net/ntsec.html#ntsec-nopasswd1

*** Info: If you want to enable that functionality, it's required to create
*** Info: a new account with special privileges (unless such an account
*** Info: already exists). This account is then used to run these special
*** Info: servers.

*** Info: Note that creating a new user requires that the current account
*** Info: have Administrator privileges itself.

*** Info: No privileged account could be found.

*** Info: This script plans to use 'cyg_server'.
*** Info: 'cyg_server' will only be used by registered services.
*** Query: Do you want to use a different name? (yes/no) no
*** Query: Create new privileged user account 'TP-20141493\cyg_server' (Cygwin name: 'tp-20141493+cyg_server')? (yes/no) yes
*** Info: Please enter a password for new user tp-20141493+cyg_server.  Please be sure
*** Info: that this password matches the password rules given on your system.
*** Info: Entering no password will exit the configuration.
*** Query: Please enter the password:
*** Query: Reenter:

*** Info: User 'tp-20141493+cyg_server' has been created with password 'gah80Xas'.
*** Info: If you change the password, please remember also to change the
*** Info: password for the installed services which use (or will soon use)
*** Info: the 'tp-20141493+cyg_server' account.


*** Info: The sshd service has been installed under the 'tp-20141493+cyg_server'
*** Info: account.  To start the service now, call `net start sshd' or
*** Info: `cygrunsrv -S sshd'.  Otherwise, it will start automatically
*** Info: after the next reboot.

*** Info: Host configuration finished. Have fun!
TP-20141493:~ % net start sshd
The CYGWIN sshd service is starting.
The CYGWIN sshd service was started successfully.

TP-20141493:~ % ps -ef | grep sshd
TP-20141    5068    3804 ?        11:06:42 /usr/sbin/sshd


myxplanetwin:

target: C:\cygwin64\bin\run.exe -quote /usr/bin/bash.exe -l -c "exec /home/philipf/src/scripts/myxplanetwin.sh"
Start in: C:\cygwin64
Normal Window

xwin:

target: C:\cygwin64\bin\run.exe --quote /usr/bin/bash.exe -l -c "cd; exec /usr/bin/startxwin"
Start in: C:\cygwin64
Run: Minimized

shared folders:

brome:~ % ls -l ~/ebw/thinkpad
lrwxrwxrwx 1 philipf philipf 35 Dec 31  2016 /home/philipf/ebw/thinkpad -> /home/philipf/vbx_shared/ebw_shared

brome:~ % ls -l vbx_shared
lrwxrwxrwx 1 philipf philipf 20 Jan  2  2017 vbx_shared -> /media/sf_vbx_shared

TP-20141493:~ % ls -l ebw/thinkpad
lrwxrwxrwx 1 philipf Domain Users 35 Jan  4  2018 ebw/thinkpad -> /home/philipf/vbx_shared/ebw_shared

TP-20141493:~ % ls -l vbx_shared/
total 12
drwxr-xr-x+ 1 philipf Domain Users 0 Oct  3 11:38 ebw_shared

systemd tunnel service:

brome:/etc/systemd/system % cat ssh-tptunnel.service 
# sudo cp ~/src/scripts/systemd/ssh-tptunnel.service /etc/systemd/system/
# sudo systemctl daemon-reload
# sudo systemctl restart ssh-tptunnel.service
# sudo systemctl status -l ssh-tptunnel.service
# sudo systemctl enable ssh-tptunnel.service

[Unit]
Description=Reverse SSH Tunnel to TP Service
ConditionPathExists=|/usr/bin
After=network.target

[Service]
User=philipf
ExecStart=/usr/bin/ssh -N -T -q -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -o StrictHostKeyChecking=no -R 20141:localhost:22 philipf@tp-20141493

# Restart every 60 seconds
RestartSec=60
Restart=always

[Install]
WantedBy=multi-user.target
